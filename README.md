Book of Tricks
==============

Create monster tents, encounter sheets and other handouts for D&D 5th Edition.

Setup
-----

```bash
pipenv install
pipenv run flask run
```

Licence
-------

This repository and the code contained within it is licensed under the [MIT
Licence][mit], and was written by [Sam Clements][@borntyping].

Favicon from the [RPG-Awesome][rpg-awesome-raw] font by [Daniela
Howe][@nagoshiashumari], licensed under the MIT licence.

The `paper.css` stylesheet is from [paper-css] by [Tsutomu Kawamura][@cognitom],
licensed under the MIT licence.

Originally started as a project to automatically fill the [Creature and
Character cards][cards] PDFs by Frank Foulis and Jack Abrasion, published on
[enworld.org][cards].

Dungeons & Dragons, D&D, etc, are property of Wizards of the Coast. This project
is provided for personal use only. No content from D&D Beyond is included with
this repository. Same as above, please let me know if this is a problem!

[@borntyping]: https://github.com/borntyping
[@cognitom]: https://github.com/cognitom
[@nagoshiashumari]: https://github.com/nagoshiashumari
[@STRML]: https://github.com/STRML
[beyond]: https://www.dndbeyond.com/
[cards]: http://www.enworld.org/forum/rpgdownloads.php?do=download&downloadid=1193
[mit]: https://opensource.org/licenses/MIT
[paper-css]: https://github.com/cognitom/paper-css/
[pdftk]: https://www.pdflabs.com/tools/pdftk-server/
[pip]: https://packaging.python.org/tutorials/installing-packages/
[Python]: https://www.python.org/
[rpg-awesome-raw]: https://github.com/nagoshiashumari/rpg-awesome-raw
