from flask import g, render_template

from flask_debugtoolbar import DebugToolbarExtension
from flask_debugtoolbar.panels import DebugPanel

__all__ = ("CreatureDebugPanel", "DebugToolbarExtension")


class CreatureDebugPanel(DebugPanel):
    name = "Creatures"

    def title(self):
        return "Creatures"

    def nav_title(self):
        return "Creatures"

    def nav_subtitle(self):
        count = len(self.loaded_creatures)
        return f"{count} {'creature' if count == 1 else 'creatures'}"

    def url(self):
        return ""

    @property
    def loaded_creatures(self):
        return g.get("loaded_creatures", [])

    @property
    def has_content(self):
        return bool(self.loaded_creatures)

    def content(self):
        return render_template(
            "flask_debugtoolbar_creatures.html.j2", creatures=self.loaded_creatures
        )
