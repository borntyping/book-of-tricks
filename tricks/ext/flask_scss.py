import warnings

warnings.filterwarnings("ignore", r"Possible nested set at position", FutureWarning)

from flask_scss import Scss

__all__ = ("Scss",)
