from typing import List

from werkzeug.routing import PathConverter


class ListConverter(PathConverter):
    def to_url(self, values: List[str]) -> str:
        return "/".join(values)

    def to_python(self, value: str) -> List[str]:
        return value.split("/")
