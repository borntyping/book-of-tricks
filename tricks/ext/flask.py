from pathlib import Path

import flask


class App(flask.Flask):
    @property
    def directory(self) -> Path:
        return Path(self.config["APP_DIRECTORY"])

    @property
    def cookies(self) -> Path:
        return self.directory / "cookies.json"

    @property
    def lookup(self) -> Path:
        return self.directory / "lookup.json"
