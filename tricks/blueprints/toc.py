import pathlib

import flask

from tricks.utils import paginate_by_size

blueprint = flask.Blueprint("toc", __name__)
blueprint.add_app_template_filter(paginate_by_size)


@blueprint.route("/")
def toc():
    return flask.render_template(
        "toc.html.j2",
        creatures=flask.current_app.creatures_cache.index,
        items=flask.current_app.items_cache.index,
    )


@blueprint.route("/images/<path:filename>")
@blueprint.route("/creatures/images/<path:filename>")
def base_static(filename):
    directory = pathlib.Path(flask.current_app.config["APP_DIRECTORY"]) / "images"
    return flask.send_from_directory(str(directory), filename)
