import typing
from logging import getLogger

import flask
import markdown
import numeral

from tricks.models.item import Equipment, MagicItem
from tricks.models.items import Chest
from tricks.modules.index import CachedIndex

log = getLogger(__name__)

blueprint = flask.Blueprint("items", __name__, url_prefix="/items")
blueprint.add_app_template_filter(numeral.int2roman)


@blueprint.before_app_first_request
def before_first_request():
    flask.current_app.items_cache = CachedIndex("items", Chest)


@blueprint.route("/<string:section>/<string:chest>")
def detail(section: str, chest: str):
    index = flask.current_app.items_cache.index
    return flask.render_template(
        "items.html.j2", section=index[section], chest=index[section][chest]
    )


@blueprint.app_template_global()
def items():
    return flask.current_app.items_cache.index


@blueprint.app_template_filter(name="sentence_case")
def _sentence_case(string: str) -> str:
    if string[0].isupper():
        return string

    return string[0].upper() + string[1:]


@blueprint.app_template_filter(name="markdown")
def _markdown(string: str) -> flask.Markup:
    return flask.Markup(markdown.markdown(string))


@blueprint.app_template_test(name="equipment")
def _equipment(value: typing.Any) -> bool:
    return isinstance(value, Equipment)


@blueprint.app_template_test(name="magic_item")
def _magic_item(value: typing.Any) -> bool:
    return isinstance(value, MagicItem)
