from logging import getLogger
from operator import attrgetter
from typing import Any, Callable, Iterable, List, Sequence, Tuple

import flask
import inflect
import natsort

from tricks.loaders.beyond import Beyond
from tricks.loaders.beyond.characters import BeyondCharacterLoader
from tricks.loaders.beyond.monsters import BeyondMonsterLoader
from tricks.loaders.custom import CustomCharacterLoader, CustomMonsterLoader
from tricks.models.creature import Character
from tricks.models.creatures import Page
from tricks.modules.index import CachedIndex

log = getLogger(__name__)

inflect_engine = inflect.engine()

blueprint = flask.Blueprint("creatures", __name__, url_prefix="/creatures")


@blueprint.before_app_first_request
def before_first_request():
    beyond = Beyond.configure(flask.current_app)

    flask.current_app.loaders = {
        "beyond/monster": BeyondMonsterLoader(beyond),
        "beyond/character": BeyondCharacterLoader(beyond),
        "custom/character": CustomCharacterLoader(),
        "custom/monster": CustomMonsterLoader(),
    }

    flask.current_app.creatures_cache = CachedIndex("creatures", Page)


@blueprint.route("/<string:section>/<string:page>/tents")
def tents(section: str, page: str):
    index = flask.current_app.creatures_cache.index
    return flask.render_template(
        "creatures_tents.html.j2", section=index[section], page=index[section][page]
    )


@blueprint.route("/<string:section>/<string:page>/encounters")
def encounters(section: str, page: str):
    index = flask.current_app.creatures_cache.index
    return flask.render_template(
        "creatures_encounters.html.j2",
        section=index[section],
        page=index[section][page],
    )


@blueprint.route("/<string:section>/<string:page>/summary")
def summary(section: str, page: str):
    index = flask.current_app.creatures_cache.index
    return flask.render_template(
        "creatures_summary.html.j2", section=index[section], page=index[section][page]
    )


@blueprint.route("/<string:section>/<string:page>/list")
def lists(section: str, page: str):
    index = flask.current_app.creatures_cache.index
    return flask.render_template(
        "creatures_list.html.j2", section=index[section], page=index[section][page]
    )


@blueprint.route("/tents")
def blank():
    return flask.render_template("tents_blank.html.j2")


@blueprint.app_template_filter()
def titlecase(_string):
    return None


@blueprint.add_app_template_test
def character(obj):
    return isinstance(obj, Character)


def partition(iterable: Iterable, func: Callable) -> Tuple[Iterable, Iterable]:
    left: List = []
    right: List = []
    for item in iterable:
        (left if func(item) else right).append(item)
    return left, right


@blueprint.app_template_filter()
def natural_sort(iterable: Iterable[Any], *attributes: str) -> Sequence[Any]:
    getter = attrgetter(*attributes)
    unsortable, sortable = partition(iterable, lambda x: getter(x) is None)
    return [*unsortable, *natsort.natsorted(sortable, key=getter)]


@blueprint.app_template_filter()
def counted_groups_of(iterable, size):
    group = []
    groups = []
    for creature in iterable:
        if (sum(c.count for c in group) + creature.count) < size:
            group.append(creature)
        else:
            groups.append(group)
            group = [creature]

    if group:
        groups.append(group)

    return groups


@blueprint.app_template_filter()
def plural(name: str, count: int) -> str:
    return inflect_engine.plural(name, count)
