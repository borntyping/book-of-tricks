import itertools
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    Optional,
    Sequence,
    Tuple,
    TypeVar,
    Union,
    Sized,
)

import attr
import slugify

A = TypeVar("A")
B = TypeVar("B")


def map_optional_func(f: Callable[[A], B]) -> Callable[[Optional[A]], Optional[B]]:
    return lambda x: map_optional(x, f)


def map_optional(x: Optional[A], f: Callable[[A], B]) -> Optional[B]:
    """Return `f(x)` if `x` is not `None`."""
    return f(x) if x is not None else x


C = TypeVar("C")


def chunks(seq: Sequence[C], size: int) -> Sequence[Sequence[C]]:
    """Yield successive n-sized chunks from l."""
    return [seq[i : i + size] for i in range(0, len(seq), size)]


def join(iterable: Iterable[Any], sep: str = ", ") -> str:
    return sep.join(str(x) for x in iterable)


K = TypeVar("K")
X = TypeVar("X")
Y = TypeVar("Y")


def convert_dict_values(d: Dict[K, X], f: Callable[[X], Y]) -> Dict[K, Y]:
    return {k: f(v) for k, v in d.items()}


def str_to_int(s: str) -> int:
    return int(strip_brackets(s.strip()))


def strip_brackets(string: str) -> str:
    if string and string.startswith("(") and string.endswith(")"):
        return string[1:-1]
    return string


def strip_period(string: str) -> str:
    return string[:-1] if string.endswith(".") else string


def split(string: str, sep: str = ",", maxsplit: int = -1) -> Tuple[str, ...]:
    return tuple(s.strip() for s in string.split(sep, maxsplit) if s.strip())


@attr.s(frozen=True)
class Split:
    string: str = attr.ib()
    sep: Optional[str] = attr.ib(default=None)
    maxsplit: int = attr.ib(default=-1)

    def __iter__(self):
        return iter(self.skip_empty())

    def _split(self, string, **kwargs):
        kwargs.setdefault("maxsplit", self.maxsplit)
        kwargs.setdefault("sep", self.sep)
        return tuple(string.split(**kwargs))

    def split(self, **kwargs) -> Tuple[str, ...]:
        return self._split(self.string, **kwargs)

    def reversed(self, **kwargs):
        """Split from the end of the string instead of from the start"""
        items = self._split(self.string[::-1], **kwargs)
        items = tuple(item[::-1] for item in items)
        return tuple(reversed(items))

    def skip_empty(self, **kwargs) -> Tuple[str, ...]:
        return tuple(x.strip() for x in self.split(**kwargs) if x.strip())

    D = TypeVar("D")

    def at_least(self, count: int, default: D = None) -> Tuple[Union[str, D], ...]:
        chain = itertools.chain(
            self.skip_empty(maxsplit=(count - 1)), itertools.repeat(default, count)
        )
        return tuple(chain)[:count]


def slug_keys(d: Dict[str, Any]) -> Dict[str, Any]:
    slugger = slugify.UniqueSlugify(to_lower=True)
    return {slugger(k): v for k, v in d.items()}


def titlecase(s: str) -> str:
    return s.replace("_", " ").replace("-", " ").title()


def acronym(s: str) -> str:
    return "".join(w[0] for w in s.split())


def paginate_by_size(sequence: Sequence[Sized], size):
    """
    Group items in a sequence by the total length of each item.

    This is used to nicely paginate sequences of variable length items for display.
    """
    page = []
    pages = []
    for item in sequence:
        new_size = sum(len(i) for i in page) + len(item)
        if not len(item):
            pass
        if new_size >= size:
            pages.append(page)
            page = [item]
        else:
            page.append(item)

    if page:
        pages.append(page)

    return pages
