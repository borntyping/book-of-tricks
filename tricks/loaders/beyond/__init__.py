import dataclasses
import json

import typing

import cachecontrol
import cachecontrol.heuristics
import cachecontrol.caches.file_cache
import requests
import requests.cookies

from tricks.ext.flask import App


@dataclasses.dataclass()
class Beyond:
    session: requests.Session = dataclasses.field(default_factory=requests.Session)
    index: typing.Dict[str, typing.Dict[str, typing.Any]] = dataclasses.field(
        default_factory=dict
    )

    @classmethod
    def configure(cls, app: App):
        index = json.loads(app.lookup.read_text())
        cookies = json.loads(app.cookies.read_text())

        # This explicitly configures a cache time, as I couldn't get
        # cachecontrol to store the responses otherwise.
        session = cachecontrol.CacheControl(
            requests.Session(),
            heuristic=cachecontrol.heuristics.ExpiresAfter(days=7),
            cache=cachecontrol.caches.file_cache.FileCache(".cache/book-of-tricks"),
        )

        for cookie in cookies:
            session.cookies.set_cookie(
                requests.cookies.create_cookie(
                    name=cookie["name"],
                    value=cookie["value"],
                    path=cookie["path"],
                    domain=cookie["domain"],
                    expires=cookie["expiry"],
                    secure=cookie["secure"],
                    rest={"HttpOnly": cookie["httpOnly"]},
                )
            )

        return cls(session=session, index=index)
