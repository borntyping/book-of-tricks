import dataclasses
import logging
import re
from typing import Any, Dict, List, Optional, Tuple

import attr
import bs4
import slugify

from tricks.loaders import CreatureLoader
from tricks.loaders.beyond import Beyond
from tricks.models.creature import Ability, Bonus, Monster, Property, Size, Speed
from tricks.models.dice import Dice
from tricks.utils import (
    Split,
    convert_dict_values,
    map_optional,
    split,
    str_to_int,
    strip_brackets,
)

log = logging.getLogger(__name__)

Kwargs = Dict[str, Any]


def find(soup: bs4.Tag, class_: str) -> Optional[str]:
    element = soup.find(class_=class_)
    return element.text.strip() if element else None


#
# Attributes (Armour class, hit points, speed)
#


@dataclasses.dataclass()
class BeyondMonsterLoader(CreatureLoader):
    """Load monster data from D&D Beyond."""

    beyond: Beyond = dataclasses.field()
    creatures: Dict[str, Monster] = dataclasses.field(default_factory=dict)

    def load(self, name, *args, **kwargs) -> Monster:
        if name not in self.creatures:
            self.creatures[name] = self.build(name, *args, **kwargs)
        return self.creatures[name]

    def build(self, name: str, options: Dict[str, Any]) -> Monster:
        log.debug(f"Scraping Beyond to build a {name!r}")
        slug = slugify.slugify(name, to_lower=True)
        url = f"https://www.dndbeyond.com/monsters/{slug}"
        response = self.beyond.session.get(url)
        response.raise_for_status()
        if getattr(response, "from_cache", False):
            log.debug(f"Loaded {url} from cache")
        else:
            log.debug(f"Fetched {url}")
        soup = bs4.BeautifulSoup(response.text, "html.parser")
        return Monster(
            **self.parse_header(soup),
            **self.parse_abilities(soup),
            **self.parse_attributes(soup),
            **self.parse_tidbits(soup),
            **self.parse_traits(soup),
            **self.parse_beyond(soup, name=name),
            url=url,
        )

    #
    # Header
    #

    @staticmethod
    def parse_header(soup: bs4.Tag) -> Kwargs:
        prefix = "mon-stat-block"
        header = soup.find(class_=f"{prefix}__header")

        if header is None:
            print(soup)

        name = header.find(class_=f"{prefix}__name-link").text.strip()
        meta = find(header, f"{prefix}__meta")
        size_and_type, alignment = Split(meta, sep=", ", maxsplit=1)
        size, m_type = Split(size_and_type, sep=" ", maxsplit=1)
        m_type = m_type.replace(" (any race)", "")
        return {
            "name": name,
            "size": Size[size.lower()],
            "type": m_type,
            "alignment": alignment,
        }

    @staticmethod
    def parse_size(string: str) -> Size:
        return Size[string]

    #
    # Abilities
    #

    @classmethod
    def parse_abilities(cls, soup: bs4.Tag) -> Kwargs:
        scores, modifiers = cls.parse_ability_blocks(soup)
        saving_throws = cls.parse_saving_throws(soup)
        return {
            name: Ability(
                score=scores[name], saving_throw=Bonus(saving_throws.get(key, 0))
            )
            for name, key in Monster.ability_names().items()
        }

    @staticmethod
    def parse_ability_blocks(soup: bs4.Tag) -> Tuple[Dict[str, int], Dict[str, int]]:
        scores = {}
        modifiers = {}
        prefix = "ability-block"

        for name, key in Monster.ability_names().items():
            element = soup.find(class_=f"{prefix}__stat--{key}")
            scores[name] = element.find(class_=f"{prefix}__score").text
            modifiers[name] = element.find(class_=f"{prefix}__modifier").text

        return (
            convert_dict_values(scores, str_to_int),
            convert_dict_values(modifiers, str_to_int),
        )

    @classmethod
    def parse_saving_throws(cls, soup: bs4.Tag) -> Dict[str, int]:
        string = cls.parse_tidbit_data(soup).get("Saving Throws", "")
        return cls.parse_bonuses(string)

    #
    # Attributes
    #

    def parse_attributes(self, soup: bs4.Tag) -> Kwargs:
        prefix = "mon-stat-block__attribute"
        attributes = {
            find(e, f"{prefix}-label"): (
                find(e, f"{prefix}-data-value"),
                map_optional(find(e, f"{prefix}-data-extra"), strip_brackets),
            )
            for e in soup.find(class_=f"{prefix}s").find_all(class_=prefix)
        }
        return {
            "armour_class": attributes["Armor Class"][0],
            "armour_type": attributes["Armor Class"][1],
            "hit_dice": Dice.from_str(attributes["Hit Points"][1]),
            **self.parse_speeds(attributes["Speed"][0]),
        }

    RE_SPEED = re.compile(
        r"(?:(?P<type>\w+) )?(?P<value>\d+) ft.(?: \((?P<extra>\w+)\))?"
    )

    @classmethod
    def parse_speeds(cls, string: str) -> Kwargs:
        # Fix for banshee: `Speed 40 ft., (hover) , walking 0 ft.`
        string = string.replace(", (", " (")
        return {"speeds": [cls.parse_speed(s) for s in Split(string, sep=",").split()]}

    @classmethod
    def parse_speed(cls, string: str) -> Speed:
        match = cls.RE_SPEED.search(string)
        assert match, f"Could not parse speed {string!r}"
        kwargs = match.groupdict()
        return Speed(value=int(kwargs.pop("value")), **kwargs)

    #
    # Tidbits (skills, senses, languages, challenge)
    #

    @dataclasses.dataclass()
    class Tidbit:
        name: str
        value: str

    @classmethod
    def parse_tidbits(cls, soup: bs4.Tag) -> Kwargs:
        tidbits = cls.parse_tidbit_data(soup)
        return {
            "damage_immunities": tidbits.get("Damage Immunities", None),
            "condition_immunities": tidbits.get("Condition Immunities", None),
            "senses": tidbits.get("Senses", None),
            **cls.parse_languages(tidbits["Languages"]),
            **cls.parse_skills(tidbits.get("Skills", "")),
            **cls.parse_challenge(tidbits["Challenge"]),
        }

    @staticmethod
    def parse_tidbit_data(soup: bs4.Tag) -> Dict[str, str]:
        prefix = "mon-stat-block__tidbit"
        return {
            find(e, f"{prefix}-label"): find(e, f"{prefix}-data")
            for e in soup.find_all(class_=prefix)
        }

    @staticmethod
    def parse_languages(string: str) -> Kwargs:
        return {"languages": (string if string != "--" else None)}

    @classmethod
    def parse_skills(cls, string: str) -> Kwargs:
        return {"skills": cls.parse_bonuses(string)}

    @staticmethod
    def parse_challenge(string: str) -> Kwargs:
        cr, xp = string.split(" ", 1)
        xp = strip_brackets(xp).replace(" XP", "")
        return {"challenge_rating": cr, "experience_points": xp}

    #
    # Traits (traits, actions, legendary actions)
    #

    @staticmethod
    def parse_traits(soup: bs4.Tag) -> Kwargs:
        prefix = "mon-stat-block__description-block"
        sections = {}

        for section in soup.find_all(class_=f"{prefix}"):
            key = find(section, class_=f"{prefix}-heading")
            sections[key]: List[Property] = []

            for e in section.find_all("p"):
                if e.find("strong"):
                    sections[key].append(
                        Property(
                            name=e.find("strong").extract().text.rstrip("."),
                            description=e.text,
                        )
                    )
                elif sections[key]:
                    # Append additional text to properties.
                    original = sections[key][-1]
                    description = " ".join((original.description, e.text))
                    sections[key][-1] = attr.evolve(original, description=description)

        for k, section in sections.items():
            assert isinstance(section, list), section
            for p in section:
                assert isinstance(p, Property), p

        return {
            "traits": sections.get(None, tuple()),
            "actions": sections.get("Actions", tuple()),
            "legendary_actions": sections.get("Legendary Actions", tuple()),
        }

    #
    # Other
    #

    @staticmethod
    def parse_bonus(string: str) -> Tuple[str, int]:
        *name, bonus = string.split(" ")
        return " ".join(name), bonus

    @classmethod
    def parse_bonuses(cls, string: str) -> Dict[str, int]:
        array = [cls.parse_bonus(bonus) for bonus in split(string)]
        return {a.lower(): Bonus(b) for (a, b) in array}

    def parse_beyond(self, soup: bs4.Tag, *, name: str) -> Kwargs:
        src = None
        source = None
        page = None

        image = soup.find(class_="monster-image")

        if image:
            src = image["src"]
            if src.startswith("//"):
                src = f"https:{src}"

        monster_source = soup.find(class_="monster-source")
        if monster_source:
            source = monster_source.text.strip()

        if source and ", pg. " in source:
            source, page = source.split(", pg. ")
            source, page = source.strip(), page.strip()

        if source == "Basic Rules":
            source = "Monster Manual"

        if page is None:
            slug = slugify.slugify(name, to_lower=True)
            page = self.beyond.index.get(slug, {}).get("page", None)

        return {"image": src, "source": source, "page": page}
