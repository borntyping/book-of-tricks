import dataclasses
import logging
import typing

from tricks.loaders import CreatureLoader
from tricks.loaders.beyond import Beyond
from tricks.models.creature import Ability, Character, Creature

log = logging.getLogger(__name__)


@dataclasses.dataclass()
class BeyondCharacterLoader(CreatureLoader):
    """Load monster data from D&D Beyond."""

    beyond: Beyond = dataclasses.field()

    def build(self, name: str, options: typing.Dict[str, typing.Any]) -> Creature:
        character_id = options.pop("id")
        url = f"https://www.dndbeyond.com/character/{character_id}/json"
        response = self.beyond.session.get(url)
        response.raise_for_status()
        character = response.json()

        race = character["race"]["baseName"]
        classes = character["classes"]
        level = sum(c["level"] for c in classes)
        cls = ", ".join(c["definition"]["name"] for c in classes)

        strength = Ability(score=character["stats"][0]["value"])
        dexterity = Ability(score=character["stats"][1]["value"])
        constitution = Ability(score=character["stats"][2]["value"])
        intelligence = Ability(score=character["stats"][3]["value"])
        wisdom = Ability(score=character["stats"][4]["value"])
        charisma = Ability(score=character["stats"][5]["value"])

        # class_features = {
        #     f["name"]
        #     for c in classes
        #     for f in c["definition"]["classFeatures"]
        #     if f["requiredLevel"] <= level
        # }
        # equipped = [i for i in character["inventory"] if i["equipped"]]
        # if "Unarmored Defense" in class_features:
        #     shields = [i for i in equipped if i["definition"]["type"] == "shield"]
        #     shields_ac = sum(i["definition"]["armorClass"] for i in shields)
        #     ac = 10 + dexterity.modifier + constitution.modifier + shields_ac
        # else:
        #     armour = [i for i in equipped if "armorClass" in i["definition"]]
        #     armour_ac = sum(i["definition"]["armorClass"] for i in armour)
        #     ac = max(armour_ac, 10) + dexterity.modifier

        character = Character(
            name=character["name"],
            type=f"{race} {cls}",
            image=character["avatarUrl"],
            hit_points=character["baseHitPoints"],
            strength=strength,
            dexterity=dexterity,
            constitution=constitution,
            intelligence=intelligence,
            wisdom=wisdom,
            charisma=charisma,
            player=character["socialName"],
            campaign=character["campaign"]["name"],
            level=level,
        )

        return character.evolve(options)
