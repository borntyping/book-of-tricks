import dataclasses
import logging
import typing

from tricks.models.creature import Creature

log = logging.getLogger(__name__)


@dataclasses.dataclass()
class CreatureLoader:
    def load(self, name: str, options: typing.Dict[str, typing.Any]) -> Creature:
        log.info(f"Loading creature {name}")
        return self.build(name, options)

    def build(self, name: str, options: typing.Dict[str, typing.Any]) -> Creature:
        raise NotImplementedError
