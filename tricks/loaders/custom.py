import dataclasses
import logging
import typing

import cattr

from tricks.loaders import CreatureLoader
from tricks.models.creature import Character, Creature, Monster

log = logging.getLogger(__name__)


@dataclasses.dataclass()
class CustomLoader(CreatureLoader):
    creature_class = Creature

    def build(self, name: str, options: typing.Dict[str, typing.Any]) -> Creature:
        options.setdefault("name", name)
        return cattr.structure(options, self.creature_class)


@dataclasses.dataclass()
class CustomMonsterLoader(CustomLoader):
    creature_class = Monster


@dataclasses.dataclass()
class CustomCharacterLoader(CustomLoader):
    creature_class = Character
