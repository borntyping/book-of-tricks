from tricks.models.dice import Dice


def test_dice():
    d = Dice.from_str("1d2+3")
    assert d.a == 1, repr(d)
    assert d.x == 2, repr(d)
    assert d.b == 3, repr(d)
