from tricks.loaders.beyond import BeyondMonsterLoader


def test_parse_bonuses():
    assert BeyondMonsterLoader.parse_bonuses("A +1, B +2") == {"a": 1, "b": 2}
