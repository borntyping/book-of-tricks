from tricks.utils import Split


class TestSplit:
    def test_split(self):
        assert Split("a b c").split() == ("a", "b", "c")

    def test_skip_empty(self):
        assert Split("a,,b,c", sep=",").skip_empty() == ("a", "b", "c")

    def test_at_least(self):
        assert Split("").at_least(2) == (None, None)
        assert Split("a").at_least(2) == ("a", None)
        assert Split("a b").at_least(2) == ("a", "b")
