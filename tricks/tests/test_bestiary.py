import flask

import tricks.blueprints.creatures


def test_creatures(app):
    tricks.blueprints.creatures.before_first_request()
    tricks.blueprints.creatures.before_request()
    assert flask.g.creatures
