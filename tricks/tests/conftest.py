import flask
import pytest

import tricks.app


@pytest.fixture()
def app() -> flask.Flask:
    return tricks.app.create_app()
