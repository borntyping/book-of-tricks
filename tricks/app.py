import logging
import os

import environs
import jinja2
import typing

import tricks.blueprints.creatures
import tricks.blueprints.items
import tricks.blueprints.toc
import tricks.ext.flask
import tricks.ext.flask_debugtoolbar
import tricks.ext.flask_scss
import tricks.ext.werkzeug
import tricks.modules.login
import tricks.modules.lookup

log = logging.getLogger(__name__)


def build_app(
    app_directory: str,
    app_beyond_username: typing.Optional[str] = None,
    app_beyond_password: typing.Optional[str] = None,
    debug_tb_enabled: bool = False,
):
    """
    The application directory should contain:
      - cookies.json
      - creatures/
      - images/
      - index.json (optional)
      - items/
    """
    app: tricks.ext.flask.App = tricks.ext.flask.App(__name__)

    # Beyond credentials used by `book-of-tricks login`.
    app.config["APP_BEYOND_USERNAME"]: str = app_beyond_username
    app.config["APP_BEYOND_PASSWORD"]: str = app_beyond_password

    # Cookies exported by `book-of-tricks login`.
    app.config["APP_BEYOND_COOKIES"]: list

    app.config["LIVE_RELOAD"] = True
    app.config["TEMPLATES_AUTO_RELOAD"] = True
    app.config["SECRET_KEY"] = "hello_world"
    app.config["APP_DIRECTORY"] = app_directory
    app.config["DEBUG_TB_ENABLED"] = debug_tb_enabled
    app.config["DEBUG_TB_PANELS"] = (
        "flask_debugtoolbar.panels.versions.VersionDebugPanel",
        "flask_debugtoolbar.panels.timer.TimerDebugPanel",
        "flask_debugtoolbar.panels.headers.HeaderDebugPanel",
        "flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel",
        "flask_debugtoolbar.panels.config_vars.ConfigVarsDebugPanel",
        "flask_debugtoolbar.panels.template.TemplateDebugPanel",
        "tricks.ext.flask_debugtoolbar.CreatureDebugPanel",
        "flask_debugtoolbar.panels.logger.LoggingPanel",
        "flask_debugtoolbar.panels.route_list.RouteListDebugPanel",
        "flask_debugtoolbar.panels.profiler.ProfilerDebugPanel",
    )

    app.jinja_env.undefined = jinja2.StrictUndefined
    app.url_map.converters["list"] = tricks.ext.werkzeug.ListConverter

    app.register_blueprint(tricks.blueprints.creatures.blueprint)
    app.register_blueprint(tricks.blueprints.toc.blueprint)
    app.register_blueprint(tricks.blueprints.items.blueprint)

    app.cli.add_command(tricks.modules.login.main)
    app.cli.add_command(tricks.modules.lookup.main)

    app.logger.setLevel(logging.WARNING)

    tricks.ext.flask_debugtoolbar.DebugToolbarExtension(app)
    tricks.ext.flask_scss.Scss(app)

    return app


def create_app():
    logging.basicConfig(level=logging.INFO, format=" * %(message)s (%(name)s)")
    logging.getLogger("werkzeug").handlers = []
    logging.getLogger("werkzeug").setLevel(logging.WARNING)
    return build_app(
        app_directory=os.environ.get("APP_DIRECTORY", "resources/pages"),
        app_beyond_username=os.environ.get("APP_BEYOND_USERNAME", None),
        app_beyond_password=os.environ.get("APP_BEYOND_PASSWORD", None),
        debug_tb_enabled=bool(os.environ.get("FLASK_DEBUG_TB_ENABLED", False)),
    )
