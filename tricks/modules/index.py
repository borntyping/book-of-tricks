import dataclasses
import logging
from collections import OrderedDict
from pathlib import Path
from typing import (
    Dict,
    Generic,
    Iterable,
    Mapping,
    Optional,
    Sequence,
    Sized,
    Type,
    TypeVar,
)

import attr
import flask
import werkzeug.utils
import yaml

from tricks.modules.named import Named
from tricks.utils import paginate_by_size

log = logging.getLogger(__name__)

T = TypeVar("T", bound="IndexEntry")


class IndexEntry(Named):
    @classmethod
    def read(cls, path: Path) -> "T":
        raise NotImplementedError


@dataclasses.dataclass()
class IndexSection(Named, Generic[T], Sized):
    name: str
    refs: Sequence[T]

    def __len__(self) -> int:
        return len(self.refs)

    def __iter__(self) -> Iterable[T]:
        return iter(self.refs)

    def __getitem__(self, item) -> T:
        return self.map[item]

    @werkzeug.utils.cached_property
    def map(self) -> Mapping[str, T]:
        return OrderedDict((x.slug, x) for x in self.refs)


@dataclasses.dataclass()
class Index(Generic[T]):
    sections: Sequence[IndexSection[T]] = attr.ib()

    def __bool__(self):
        return bool(self.sections)

    def __iter__(self):
        return iter(self.sections)

    def __getitem__(self, item) -> IndexSection[T]:
        return self.map[item]

    @werkzeug.utils.cached_property
    def map(self) -> Mapping[str, IndexSection[T]]:
        return OrderedDict((x.slug, x) for x in self.sections)


@dataclasses.dataclass()
class CachedIndex(Generic[T]):
    name: str
    entry_cls: Type[T]

    _index: Index[T] = dataclasses.field(default=None)
    _mtime: Optional[int] = dataclasses.field(default=None)

    def __init__(self, name: str, entry_cls: Type[T]):
        self.name = name
        self.entry_cls = entry_cls

    @property
    def directory(self) -> Path:
        return flask.current_app.directory / self.name

    @property
    def index(self):
        path = self.directory / "index.yaml"
        data = yaml.safe_load(path.read_text())

        # Find the latest mtime in the files.
        log.info(f"Checking if files referenced by index {path} have updated")
        mtime: Optional[int] = None
        paths = [path] + [self.directory / ref for x in data for ref in x["refs"]]
        for path in paths:
            path_mtime = path.stat().st_mtime_ns
            if mtime is None or path_mtime > mtime:
                mtime = path_mtime

        if self._mtime is None or self._mtime < mtime:
            log.warning(f"Files have updated, reloading index {path}")
            self._mtime = mtime
            self._index = Index(sections=[self.section(o) for o in data])
        else:
            log.info(f"Using cached index from {path}")

        return self._index

    def section(self, data: Dict) -> IndexSection[T]:
        return IndexSection(
            name=data["name"],
            refs=[self.entry_cls.read(self.directory / r) for r in data["refs"]],
        )
