import json
import pathlib

import click
import flask
import mypy_extensions
import slugify

Reference = mypy_extensions.TypedDict(
    "Reference", {"name": str, "source": str, "page": int}
)


def build_index(repo: pathlib.Path, output: pathlib.Path) -> None:
    directory = pathlib.Path(repo) / "data" / "bestiary"

    i = {}
    for path in directory.glob("bestiary-*.json"):
        data = json.loads(path.read_text())
        for monster in data["monster"]:
            i[slugify.slugify(monster["name"], to_lower=True)]: Reference = {
                "name": monster["name"],
                "source": monster["source"],
                "page": monster.get("page"),
            }

    output.write_text(json.dumps(i, sort_keys=True, indent=2))


@click.command(name="lookup")
@click.option("-r", "--repo", type=click.Path(exists=True))
def main(repo):
    """Generate a lookup index from a set of bestiary-*.json files."""
    build_index(pathlib.Path(repo), flask.current_app.lookup)
