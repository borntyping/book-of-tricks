import json

import click
import flask
import flask.cli
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class SeleniumIFrame:
    driver: Firefox
    name: str

    def __init__(self, driver, name):
        self.driver = driver
        self.name = name

    def __enter__(self) -> None:
        self.driver.switch_to.frame(self.name)

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.driver.switch_to.default_content()


class Selenium:
    def __enter__(self) -> "Selenium":
        self.driver = Firefox()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.driver.close()

    def iframe(self, name: str) -> SeleniumIFrame:
        return SeleniumIFrame(driver=self.driver, name=name)

    def wait_for_class(self, class_name: str, timeout=10) -> None:
        WebDriverWait(self.driver, timeout).until(
            EC.presence_of_element_located((By.CLASS_NAME, class_name))
        )


def login_beyond(username: str, password: str) -> dict:
    with Selenium() as s:
        s.driver.get("https://www.dndbeyond.com/login")
        s.wait_for_class("twitch-container")
        with s.iframe("passport"):
            s.wait_for_class("js-login-button")
            s.driver.find_element_by_id("username").send_keys(username)
            s.driver.find_element_by_name("password").send_keys(password)
            s.driver.find_element_by_class_name("js-login-button").click()
        s.wait_for_class("user-interactions-profile-nickname")
        return s.driver.get_cookies()


@click.command(name="login")
@click.option("--username", prompt=True)
@click.option("--password", prompt=True)
def main(username, password):
    """Login to D&D Beyond with Selenium and save cookies."""
    with Selenium() as s:
        s.driver.get("https://www.dndbeyond.com/login")
        s.wait_for_class("twitch-container")
        with s.iframe("passport"):
            s.wait_for_class("js-login-button")
            s.driver.find_element_by_id("username").send_keys(username)
            s.driver.find_element_by_name("password").send_keys(password)
            s.driver.find_element_by_class_name("js-login-button").click()
        s.wait_for_class("user-interactions-profile-nickname")
        cookies = s.driver.get_cookies()

    flask.current_app.cookies.write_text(json.dumps(cookies, indent=2))
