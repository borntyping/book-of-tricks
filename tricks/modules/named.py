import slugify


class Named:
    name: str

    @property
    def slug(self):
        return slugify.slugify(self.name.lower())
