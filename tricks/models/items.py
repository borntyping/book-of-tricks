from pathlib import Path
from typing import Iterable, Sequence, TypeVar, Any, Optional

import attr
import cattr
import yaml

from tricks.modules.index import IndexEntry
from tricks.utils import chunks
from .item import Item

V = TypeVar("V")


@attr.s(frozen=True, kw_only=True)
class Chest(IndexEntry):
    """Loaded from a single file in the index."""

    name: str = attr.ib()
    items: Sequence[Item] = attr.ib()
    meta: Optional[Any] = attr.ib(default=None)

    def __iter__(self) -> Iterable[Item]:
        return iter(self.items)

    def cards(self, size: int) -> Iterable[Iterable[Item]]:
        return chunks([i for i in self.items for _ in range(0, i.count)], size)

    converter = cattr.Converter()
    converter.register_structure_hook(Item, Item.structure)

    @classmethod
    def read(cls, path: Path):
        return cls.converter.structure(yaml.safe_load(path.read_text()), cls)
