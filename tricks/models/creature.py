import enum
import logging
import operator
import re
from copy import copy
from typing import Any, Dict, Optional, Sequence, Tuple, Type

import attr
import cattr
import frozendict
import werkzeug.utils

from tricks.models.dice import Dice
from tricks.modules.named import Named

log = logging.getLogger(__name__)

MODIFIERS = {
    1: -5,
    2: -4,
    3: -4,
    4: -3,
    5: -3,
    6: -2,
    7: -2,
    8: -1,
    9: -1,
    10: +0,
    11: +0,
    12: +1,
    13: +1,
    14: +2,
    15: +2,
    16: +3,
    17: +3,
    18: +4,
    19: +4,
    20: +5,
    21: +5,
    22: +6,
    23: +6,
    24: +7,
    25: +7,
    26: +8,
    27: +8,
    28: +9,
    29: +9,
    30: +10,
}

SOURCES = {
    "Basic Rules": "MM",
    "Monster Manual": "MM",
    "Curse of Strahd": "CoS",
    "Tomb of Annihilation": "ToA",
    "Lost Mine of Phandelver": "LMoP",
    "Tales From The Yawning Portal": "TFTYP",
    "The Sunless Citadel": "SC",
    "Waterdeep: Dragon Heist": "DH",
    "Tomb of Horrors": "ToH",
    "Volo's Guide to Monsters": "VGM",
    "Storm King's Thunder": "SKT",
    "Out of the Abyss": "OOTA",
    "Mordenkainen’s Tome of Foes": "MToF",
    "Hoard of the Dragon Queen": "HDQ",
    # Third party
    "Shore of Dreams": "SD",
    "Tower of the Mad Mage": "ToTMM",
}


def convert_alignment(alignment: Optional[str]) -> Optional[str]:
    return alignment if alignment != "unaligned" else None


class Size(enum.Enum):
    tiny = "tiny"
    small = "small"
    medium = "medium"
    large = "large"
    huge = "huge"
    gargantuan = "gargantuan"


class Bonus(int):
    """An integer that displays a +/- modifier when converted to a string."""

    def __str__(self):
        return f"{self:+d}"


@attr.s(frozen=True, kw_only=True)
class Speed:
    """A movement speed in feet."""

    value: int = attr.ib()
    type: Optional[str] = attr.ib(default=None)
    extra: Optional[str] = attr.ib(default=None)

    def __str__(self):
        string = f"{self.value} ft."
        if self.type:
            string = f"{self.type} {string}"
        if self.extra:
            string = f"{string} ({self.extra})"
        return string


@attr.s(frozen=True, kw_only=True)
class Ability:
    """A single ability score and modifier."""

    score: int = attr.ib(default=10)
    saving_throw: Bonus = attr.ib(default=0)

    @property
    def modifier(self) -> Bonus:
        return Bonus(MODIFIERS[self.score])


@attr.s(frozen=True, kw_only=True)
class Property:
    name: str = attr.ib()
    description: Optional[str] = attr.ib(cmp=False, repr=False, default=None)
    display: bool = attr.ib(default=True, convert=bool)

    def __bool__(self):
        return bool(self.description) and self.display


@attr.s(frozen=True, kw_only=True)
class Creature(Named):
    name: str = attr.ib()
    size: Size = attr.ib(default=Size.medium)
    type: Optional[str] = attr.ib(default=None)
    alignment: Optional[str] = attr.ib(default="Neutral", converter=convert_alignment)

    # Attributes
    armour_class: Optional[int] = attr.ib(default=None)
    armour_type: Optional[str] = attr.ib(default=None)
    hit_dice: Optional[Dice] = attr.ib(convert=Dice.from_object, default=None)
    hit_points: Optional[int] = attr.ib(default=None)
    speeds: Tuple[Speed, ...] = attr.ib(convert=tuple, default=attr.Factory(tuple))

    # Abilities
    strength: Ability = attr.ib(factory=Ability)
    dexterity: Ability = attr.ib(factory=Ability)
    constitution: Ability = attr.ib(factory=Ability)
    intelligence: Ability = attr.ib(factory=Ability)
    wisdom: Ability = attr.ib(factory=Ability)
    charisma: Ability = attr.ib(factory=Ability)

    # Tidbits
    # Note: challenge_rating can be a fraction.
    damage_resistances: Optional[str] = attr.ib(default=None)
    damage_immunities: Optional[str] = attr.ib(default=None)
    damage_vulnerabilities: Optional[str] = attr.ib(default=None)
    condition_immunities: Optional[str] = attr.ib(default=None)
    skills: Dict[str, Bonus] = attr.ib(
        convert=frozendict.frozendict, default=attr.Factory(dict)
    )
    senses: Optional[str] = attr.ib(default=None)
    languages: Optional[str] = attr.ib(default=None)
    challenge_rating: Optional[str] = attr.ib(default=None)
    experience_points: Optional[int] = attr.ib(default=None)

    # Traits and actions.
    traits: Tuple[Property, ...] = attr.ib(convert=tuple, default=attr.Factory(tuple))
    actions: Tuple[Property, ...] = attr.ib(convert=tuple, default=attr.Factory(tuple))

    # Information from the source - image and references.
    url: Optional[str] = attr.ib(default=None)
    source: Optional[str] = attr.ib(default=None)
    image: Optional[str] = attr.ib(default=None)
    page: Optional[int] = attr.ib(default=None)

    # Set to the creature this is based on by the loader.
    original: Optional["Creature"] = attr.ib(default=None, repr=False)

    # Extra CSS classes.
    classes: Sequence[str] = attr.ib(convert=tuple, default=attr.Factory(tuple))

    @werkzeug.utils.cached_property
    def notable_senses(self):
        match = re.search(r"Passive Perception \d+", self.senses)

        if match:
            return re.sub(r",\s+Passive Perception \d+", "", self.senses)

        return self.senses

    @property
    def speed(self) -> Optional[Speed]:
        return self.speeds[0] if self.speeds else None

    @property
    def initiative(self) -> int:
        return 10 + self.dexterity.modifier

    @classmethod
    def ability_names(cls) -> Dict[str, str]:
        return {
            "strength": "str",
            "dexterity": "dex",
            "constitution": "con",
            "intelligence": "int",
            "wisdom": "wis",
            "charisma": "cha",
        }

    def abilities(self):
        return {
            "str": self.strength,
            "dex": self.dexterity,
            "con": self.constitution,
            "int": self.intelligence,
            "wis": self.wisdom,
            "cha": self.charisma,
        }

    @property
    def skills_list(self):
        return [f"{key.title()} {value}" for key, value in self.skills.items()]

    @property
    def saving_throws(self):
        return [
            f"{name.upper()} {ability.saving_throw}"
            for name, ability in self.abilities().items()
            if ability and ability.saving_throw
        ]

    @property
    def reference(self) -> Optional[str]:
        if self.source and self.page:
            return f"{self.source_name}{self.page}"

        if self.source:
            return self.source_name

        return None

    def references(self) -> Sequence[str]:
        if self.reference:
            yield self.reference

        if self.original and self.original.reference:
            yield f"{self.original.reference} ({self.original.name})"

    @property
    def source_name(self) -> Optional[str]:
        """
        This used to call `acronym(self.source)`.
        :return:
        """
        if self.source in SOURCES.keys():
            return SOURCES[self.source]
        elif self.source in SOURCES.values():
            return self.source
        elif self.source:
            return self.source
        return None

    @property
    def is_from_monster_manual(self) -> bool:
        return self.source in ("Basic Rules", "Monster Manual")

    @property
    def min_hit_points(self) -> Optional[int]:
        if self.hit_points is not None:
            return None

        if self.hit_dice is not None:
            return max(self.hit_dice.a, self.hit_dice.min)

        return None

    @property
    def avg_hit_points(self) -> Optional[int]:
        if self.hit_points is not None:
            return self.hit_points

        if self.hit_dice is not None:
            return max(self.hit_dice.a, self.hit_dice.avg)

        return None

    @property
    def max_hit_points(self) -> Optional[int]:
        if self.hit_points is not None:
            return None

        if self.hit_dice is not None:
            return max(self.hit_dice.a, self.hit_dice.max)

        return None

    def debug(self):
        values = attr.asdict(self, recurse=False)
        raw = attr.asdict(self, recurse=True)

        for key in attr.fields(self.__class__):
            yield key.name, values[key.name], raw[key.name]

    @staticmethod
    def _modify(mapping: Dict, key: str, cls: Type) -> None:
        if key in mapping:
            mapping[key] = cattr.structure(mapping[key], cls)

    def evolve(self, fields: Dict, **kwargs):
        for f in attr.fields(self.__class__):
            if f.name in fields:
                fields[f.name] = cattr.structure(fields[f.name], f.type)

        return attr.evolve(self, **fields, **kwargs)

    def modify(self, changes: Dict):
        changes = copy(changes)

        # Erase the source of modified creatures.
        # Use creature.original to get the source if needed.
        changes.setdefault("source", None)
        changes.setdefault("page", None)

        return self.evolve(changes, original=self)

    def compare(self, attribute: str) -> Tuple[bool, Any]:
        """
        Return true if old and new values are different, false if old and new
        values are the same, and false if there is no original creature to
        compare to.
        """

        getter = operator.attrgetter(attribute)

        if self.original is None:
            return False, getter(self)

        new = getter(self)
        old = getter(self.original)

        return new != old, new

    def challenge_rating_unicode(self) -> str:
        return {"1/8": "⅛", "1/4": "¼", "1/2": "½"}.get(
            self.challenge_rating, self.challenge_rating
        )


@attr.s(frozen=True, kw_only=True)
class Monster(Creature):
    legendary_actions: Tuple[Property, ...] = attr.ib(
        convert=tuple, default=attr.Factory(tuple)
    )

    @property
    def legendary(self):
        return bool(self.legendary_actions)

    def description(self, include_alignment: bool = False) -> Optional[str]:
        desc = []

        if self.legendary:
            desc.append("Legendary")

        if self.size:
            desc.append(self.size.value)

        if self.type:
            desc.append(self.type)

        desc_str = " ".join(desc)

        if self.alignment and include_alignment:
            desc_str += f", {self.alignment}"

        return desc_str.capitalize()


@attr.s(frozen=True, kw_only=True)
class Character(Monster):
    player: Optional[str] = attr.ib(default=None)
    campaign: Optional[str] = attr.ib(default=None)
    level: Optional[int] = attr.ib(default=None)
    theme: Optional[str] = attr.ib(default=None)

    spell_save_dc: Optional[int] = attr.ib(default=None)
    passive_perception: Optional[int] = attr.ib(default=None)
