from pathlib import Path
from typing import Any, Dict, List, Optional, Sequence, Set, Type, Tuple, Sized

import attr
import cattr
import flask
import werkzeug.utils
import yaml

from tricks.modules.index import IndexEntry
from tricks.modules.named import Named
from tricks.utils import chunks, paginate_by_size
from .creature import Creature


def modify(mapping: Dict, key: str, cls: Type) -> None:
    if key in mapping:
        mapping[key] = cattr.structure(mapping[key], cls)


@attr.s(frozen=True)
class Settings:
    tents: int = attr.ib(default=1)
    order: int = attr.ib(default=0)
    notes: Optional[str] = attr.ib(default=None, cmp=False)
    classes: Sequence[str] = attr.ib(factory=tuple, convert=tuple)
    summarize: bool = attr.ib(default=True)


@attr.s(frozen=True)
class Reference(Named):
    name: str = attr.ib()
    loader: str = attr.ib(default="beyond/monster")
    options: Dict[str, Any] = attr.ib(factory=dict)
    modify: Dict[str, Any] = attr.ib(factory=dict)
    corrections: Dict[str, Any] = attr.ib(factory=dict)
    settings: Settings = attr.ib(factory=Settings)

    count: int = attr.ib(default=1)

    @werkzeug.utils.cached_property
    def creature(self) -> Creature:
        loader = flask.current_app.loaders[self.loader]
        creature = loader.load(self.name, self.options)

        if self.corrections:
            creature = creature.evolve(self.corrections)

        if self.modify:
            creature = creature.modify(self.modify)

        flask.g.setdefault("loaded_creatures", [])
        flask.g.loaded_creatures.append(creature)

        return creature


@attr.s(frozen=True)
class Tent:
    """
    A compiled reference that can be passed to the tents templates and de-duplicated in
    a set. Used to sort tents by their ordering and creature name.
    """

    creature: Creature = attr.ib()
    settings: Settings = attr.ib()
    unique: int = attr.ib()

    def key(self):
        return self.settings.order, self.creature.name, self.unique


@attr.s()
class Encounter(Named, Sized):
    name: str = attr.ib()
    area: Optional[str] = attr.ib(default=None)
    creatures: List[Reference] = attr.ib(default=attr.Factory(list))

    def __len__(self) -> int:
        return self.count

    @property
    def count(self) -> int:
        return sum(reference.count for reference in self.creatures)


@attr.s()
class Page(IndexEntry):
    name: str = attr.ib()
    encounters: Sequence[Encounter] = attr.ib()
    creatures: Any = attr.ib(default=None)
    meta: Any = attr.ib(default=None)

    @classmethod
    def read(cls, path: Path):
        return cattr.structure(yaml.safe_load(path.read_text()), cls)

    def tents(self, size: int):
        """
        This deduplicates creatures included in the page.

        If a reference asks for multiple tents, creatures will be included
        multiple times up to the _maximum_ number requested by a reference.
        """
        tents = {
            Tent(creature=ref.creature, settings=ref.settings, unique=unique)
            for encounter in self.encounters
            for ref in encounter.creatures
            for unique in range(ref.settings.tents)
        }

        return chunks(tuple(sorted(tents, key=Tent.key)), size)

    def paged_encounters(self, size):
        return paginate_by_size(self.encounters, size)

        group = []
        groups = []
        for encounter in self.encounters:
            new_size = sum(c.count for c in group) + encounter.count
            if (not encounter.creatures) or (new_size >= size):
                groups.append(group)
                group = [encounter]
            else:
                group.append(encounter)

        if group:
            groups.append(group)

        return [g for g in groups if g]

    def areas(self) -> Set[str]:
        return {encounter.area for encounter in self.encounters}

    def encounter_names(self) -> Set[str]:
        return {encounter.name for encounter in self.encounters}

    def sources(self) -> Set[Tuple[str, str]]:
        return {
            (ref.creature.source, ref.creature.page)
            for encounter in self.encounters
            for ref in encounter.creatures
        }
