import math
import re
from logging import getLogger
from typing import Optional

import attr

log = getLogger(__name__)


@attr.s(frozen=True)
class Dice:
    a: int = attr.ib()  # Number
    x: int = attr.ib()  # Sides
    b: int = attr.ib(default=0)  # Modifier

    def __str__(self):
        return f"{self.a}d{self.x}{self.b:+d}"

    def __bool__(self):
        return not (self.a == 0 and self.b == 0 and self.x == 0)

    @property
    def min(self):
        return self.a * 1 + self.b

    @property
    def max(self):
        return self.a * self.x + self.b

    @property
    def avg(self):
        return math.floor((self.min + self.max) / 2)

    @classmethod
    def from_str(cls, string: str) -> "Dice":
        assert isinstance(string, str), repr(string)
        string = re.sub(r"\s+", "", string)
        match = re.match(r"^(?P<a>\d+)d(?P<x>\d+)(?P<b>[\+\-]\d+)?$", string)
        assert match, f"Could not parse {string!r} as dice notation"
        args = match.groupdict(default=0)
        dice = cls(a=int(args["a"]), x=int(args["x"]), b=int(args["b"]))
        return dice

    @classmethod
    def from_object(cls, value) -> Optional["Dice"]:
        if isinstance(value, Dice):
            return value
        elif isinstance(value, str):
            return cls.from_str(value)
        elif value is None:
            return None
        raise NotImplementedError
