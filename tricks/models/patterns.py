from textwrap import dedent
from typing import Any, Optional

import attr

from .item import Pattern, MagicItemRarity


@attr.s(frozen=True)
class HealingPotionPattern(Pattern, name="healing-potion"):
    type: Optional[str] = attr.ib(default=None)
    count: int = attr.ib(default=1)

    potions = {
        None: ("Healing", "common", (2, 4, 2)),
        "greater": ("Greater Healing", "uncommon", (4, 4, 4)),
        "superior": ("Superior Healing", "rare", (8, 4, 8)),
        "supreme": ("Supreme Healing", "very rare", (10, 4, 20)),
    }

    def data(self):
        name, rarity, (a, x, b) = self.potions[self.type]

        return {
            "name": f"Potion of {name}",
            "count": self.count,
            "type": "potion",
            "rarity": rarity,
            "attributes": [{"name": "Hit points restored", "value": f"{a}d{x}+{b}"}],
            "dice": {"a": a, "x": x, "b": b},
            "description": dedent(
                f"""
                You regain **{a}d{x}+{b} hit points** when you drink this
                potion.
                
                The potion’s red liquid glimmers when agitated.
            """
            ),
            "reference": "Players Handbook, pg. 187",
        }


@attr.s(frozen=True)
class SpellScrollPattern(Pattern, name="scroll"):
    spell: str = attr.ib()
    level: int = attr.ib()
    count: int = attr.ib(default=1)
    cast: Optional[int] = attr.ib(
        default=attr.Factory(lambda self: self.level, takes_self=True)
    )
    description: str = attr.ib(
        default=dedent(
            """
        A spell scroll bears the words of a single spell, written in a mystical
        cipher. If the spell is on your class’s spell list, you can read the
        scroll and cast its spell without providing any material components.
        Once the spell is cast, the words on the scroll fade, and it crumbles
        to dust.
    """
        )
    )

    RARITY = {
        0: MagicItemRarity.COMMON,
        1: MagicItemRarity.COMMON,
        2: MagicItemRarity.UNCOMMON,
        3: MagicItemRarity.UNCOMMON,
        4: MagicItemRarity.RARE,
        5: MagicItemRarity.RARE,
        6: MagicItemRarity.VERY_RARE,
        7: MagicItemRarity.VERY_RARE,
        8: MagicItemRarity.VERY_RARE,
        9: MagicItemRarity.LEGENDARY,
    }
    SAVE_DC = {0: 13, 1: 13, 2: 13, 3: 15, 4: 15, 5: 17, 6: 17, 7: 18, 8: 18, 9: 19}
    ATTACK_BONUS = {0: 5, 1: 5, 2: 5, 3: 7, 4: 7, 5: 9, 6: 9, 7: 10, 8: 10, 9: 11}

    @property
    def rarity(self) -> str:
        return self.RARITY[self.level].value

    @property
    def attack_bonus(self) -> int:
        return self.ATTACK_BONUS[self.level]

    @property
    def save_dc(self) -> int:
        return self.SAVE_DC[self.level]

    def data(self) -> Any:
        return {
            "name": f"Spell Scroll of <em>{self.spell}</em>",
            "type": "scroll",
            "rarity": self.rarity,
            "attributes": [
                {
                    "name": "Scroll level",
                    "value": self.cast,
                    "modified": self.cast != self.level,
                },
                {"name": "Save DC", "value": str(self.save_dc)},
                {"name": "Spell level", "value": str(self.level)},
                {"name": "Attack Bonus", "value": f"+{self.attack_bonus}"},
            ],
            "description": self.description,
            "reference": "Players Handbook, pg. 200",
        }
