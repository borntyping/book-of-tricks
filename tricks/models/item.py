from enum import Enum, unique
from logging import getLogger
from typing import Any, Dict, Mapping, Optional, Sequence, Type, Union

import attr
import cattr

from tricks.models.dice import Dice
from tricks.modules.named import Named

log = getLogger(__name__)


@unique
class MagicItemRarity(Enum):
    COMMON = "common"
    UNCOMMON = "uncommon"
    RARE = "rare"
    VERY_RARE = "very rare"
    LEGENDARY = "legendary"


class ItemType:
    def default_icon_for_type(self) -> Optional[str]:
        raise NotImplemented


@unique
class EquipmentType(ItemType, Enum):
    OTHER = "other"
    ARMOUR = "armour"
    WEAPON = "weapon"

    def default_icon_for_type(self) -> Optional[str]:
        icons = {self.ARMOUR: "ra-corked-tube"}

        return icons.get(self)


@unique
class MagicItemType(ItemType, Enum):
    ARMOUR = "armour"
    POTION = "potion"
    RING = "ring"
    ROD = "rod"
    SCROLL = "scroll"
    STAFF = "staff"
    WAND = "wand"
    WEAPON = "weapon"
    WONDROUS_ITEM = "wondrous item"

    def default_icon_for_type(self) -> Optional[str]:
        icons = {
            "ARMOUR": None,
            "POTION": "ra-corked-tube",
            "RING": None,
            "ROD": None,
            "SCROLL": "ra-scroll-unfurled",
            "STAFF": None,
            "WAND": "ra-fairy-wand",
            "WEAPON": "ra-broadsword",
            "WONDROUS_ITEM": "ra-emerald",
        }

        return icons.get(self.name)


@attr.s(frozen=True)
class Attribute:
    name: str = attr.ib()
    value: str = attr.ib()
    modified: bool = attr.ib(default=False)


class Pattern:
    def __init_subclass__(cls, name: str) -> None:
        Item.patterns[name] = cls


class Missing:
    def __bool__(self):
        return False


missing = Missing()


@attr.s(frozen=True, kw_only=True)
class Item(Named):
    type: ItemType

    name: str = attr.ib()
    attributes: Optional[Sequence[Attribute]] = attr.ib(factory=tuple)
    description: Optional[str] = attr.ib(default=None)
    reference: Optional[str] = attr.ib(default=None)
    image: Optional[str] = attr.ib(default=None)
    background: Optional[str] = attr.ib(default=None)
    icon: Optional[str] = attr.ib(default=missing)
    dice: Optional[Dice] = attr.ib(converter=Dice.from_object, default=None)
    count: int = attr.ib(default=1)

    classes: Sequence[str] = attr.ib(factory=tuple)
    subtitle: Optional[str] = attr.ib(default=None)

    version: int = 6

    patterns: Dict[str, Type[Pattern]] = {}
    subclasses: Dict[str, Type["Item"]] = {}

    def __init_subclass__(cls, *, name: str, **kwargs):
        cls.subclasses[name] = cls

    def icon_or_default(self):
        if self.icon is missing:
            return self.type.default_icon_for_type()

        return self.icon

    @classmethod
    def structure(cls, data: Any, _type: type) -> "Item":
        if "pattern" in data:
            pattern_cls: type = cls.patterns[data.pop("pattern")]
            pattern: Any = cattr.structure(data, pattern_cls)
            data: Mapping = pattern.data()

        return cattr.structure(data, cls.subclasses[data.pop("sort", "magic")])


@attr.s(frozen=True, kw_only=True)
class MagicItem(Item, name="magic"):
    type: Optional[MagicItemType] = attr.ib()
    rarity: Optional[MagicItemRarity] = attr.ib(default=MagicItemRarity.COMMON)
    attunement: bool = attr.ib(default=False)


@attr.s(frozen=True, kw_only=True)
class Equipment(Item, name="equipment"):
    type: Optional[EquipmentType] = attr.ib(default=EquipmentType.OTHER)
    cost: Optional[str] = attr.ib(default=None)
